// TODO: Move form to components
import React, { useState } from "react";

import {
  Container,
  Row,
  Col,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Alert,
} from "reactstrap";
import { Formik, Form } from "formik";
import { string, object } from "yup";

import PageHead from "../components/PageHead";
import { MedicationRequest } from "../types";
import { axiosInstance } from "../utils/api";

const initialValues = {
  city: "",
  name: "",
  mobile: "",
  requirement: "",
};

const RequestSchema = object().shape({
  city: string().trim().required("Город обязателен для заполнения"),
  name: string().trim().required("Имя обязательно для заполнения"),
  mobile: string().trim().required("Номер телефона обязателен для заполнения"),
  requirement: string()
    .trim()
    .required("Текст просьбы обязателен для заполнения"),
});

export const Request = () => {
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [submitError, setSubmitError] = useState(null);

  const handleSubmit = async (values: MedicationRequest, actions: any) => {
    console.log(values);
    try {
      const data = await axiosInstance.post("/request/submit", values);

      console.log("handleSubmit -> data", data);
      actions.setSubmitting(false);
      setIsSubmitted(true);
      actions.resetForm();
    } catch (error) {
      console.log("handleSubmit -> error", error);
      actions.setSubmitting(false);
      setIsSubmitted(false);
      actions.resetForm();
      setSubmitError(error.message);
    }
  };

  return (
    <Container>
      <PageHead title="Мне нужна помощь" />
      <Row className={"justify-content-center"}>
        <Col md={6}>
          <h1 className={"text-center mb-4"} >Форма запроса помощи</h1>
          <Alert color="info" className="text-center">
            <b>Заполните форму максимально подробно и перепроверьте перед отправкой. Заполняя форму, вы даете согласие на обработку и хранение ваших персональных данных.</b>
          </Alert>
          <Alert color="primary" className="text-center">
            <b>ПРОСЬБА: Отправлять форму ТОЛЬКО ОДИН РАЗ.</b>
          </Alert>
          <Alert color="warning" className="text-center">
            <b>Коммерческая реализация лекарственных средств без лицензии недопустима. Подобные заявки будут удаляться с веб-сайта.</b>
          </Alert>
          <Formik
            initialValues={initialValues}
            onSubmit={handleSubmit}
            validationSchema={RequestSchema}
          >
            {({ errors, touched, handleChange, handleBlur, isSubmitting }) => (
              <Form>
                <FormGroup>
                  <Label for="city" size="lg">
                    Город*
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="text"
                      name="city"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      id="city"
                      placeholder="Город"
                      bsSize="lg"
                      invalid={touched.city && !!errors.city}
                    />
                    {touched.city && errors.city && (
                      <FormFeedback tooltip>{errors.city}</FormFeedback>
                    )}
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Label size="lg" for="mobile">
                    Номер телефона*
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="tel"
                      name="mobile"
                      id="mobile"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="Номер телефона"
                      bsSize="lg"
                      invalid={touched.mobile && !!errors.mobile}
                    />
                    {touched.mobile && errors.mobile && (
                      <FormFeedback tooltip>{errors.mobile}</FormFeedback>
                    )}
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Label size="lg" for="name">
                    Ваше имя*
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="text"
                      name="name"
                      id="name"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="Имя"
                      bsSize="lg"
                      invalid={touched.name && !!errors.name}
                    />
                    {touched.name && errors.name && (
                      <FormFeedback tooltip>{errors.name}</FormFeedback>
                    )}
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Label size="lg" for="requirement">
                    Ваш запрос/просьба*
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="textarea"
                      name="requirement"
                      id="requirement"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="Текст запроса/просьбы"
                      bsSize="lg"
                      invalid={touched.requirement && !!errors.requirement}
                    />
                    {touched.requirement && errors.requirement && (
                      <FormFeedback tooltip>{errors.requirement}</FormFeedback>
                    )}
                  </Col>
                </FormGroup>
                <FormGroup row className="mt-5">
                  <Col sm={10}>
                    <Button
                      className="mb-3"
                      disabled={
                        !!Object.keys(errors).length ||
                        isSubmitting ||
                        isSubmitted
                      }
                      type="submit"
                      size="lg"
                      color={isSubmitted ? "success" : "primary"}
                    >
                      {isSubmitted ? "Запрос отправлен" : "Отправить запрос"}
                    </Button>
                    {submitError ? (
                      <Alert color="danger">{submitError}</Alert>
                    ) : null}
                  </Col>
                </FormGroup>
              </Form>
            )}
          </Formik>
        </Col>
      </Row>
    </Container>
  );
};
